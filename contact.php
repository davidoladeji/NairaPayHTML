<!DOCTYPE html>
<html>
<head lang="en">
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui"/>

    <!-- FONTS -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css"  rel="stylesheet" >
    <link href='https://fonts.googleapis.com/css?family=Poppins:300,400,700' rel='stylesheet' type='text/css'>

    <!-- CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous" rel="stylesheet">
    <link href="css/idangerous.swiper.css" rel="stylesheet" type="text/css" />
    <link href="css/mainstyle.css" rel="stylesheet" type="text/css" />

    <link rel="shortcut icon" href="img/favicon.ico" />
    <title>Naira Pay Naija | Split-second payments</title>
</head>
<body>

    <!-- LOADER -->
    <div id="loader-wrapper">
        <div id="loader"><span class="loader-inner"></span></div>
        <div id="loading-text">LOADING</div>
    </div>

    <!-- HEADER -->
    <header class="tt-header">
        <div class="container">
            <div class="top-inner clearfix">
                <div class="top-inner-container">
                    <a class="logo" href="index.php"><img src="img/header/logo.png" height="38" width="51" alt=""></a>
                    <button class="cmn-toggle-switch"><span></span></button>
                </div>
            </div>
            <div class="toggle-block">
                <div class="toggle-block-container">
                    <nav class="main-nav clearfix">
                        <ul>
                            <li class="active">
                                <a href="index.php">Home</a>
                            </li>
                            <li class="parent">
                                <a href="index.php#standout">About <i class="menu-toggle fa fa-angle-down"></i></a>
                            </li>

                            <li><a href="index.php#packages">Packages</a></li>

                            <!--<li><a href="index.php#faq">FAQ</a></li>-->
                            <li><a href="contact.php">Contact</a></li>
                        </ul>
                    </nav>
                    <div class="nav-more">
                        <a class="c-btn type-2" href="/user/login"><span>Login</span></a>
                    </div>
                    <div class="nav-more">
                        <a class="c-btn type-2" href="/user/register"><span>Register</span></a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="tt-header-margin"></div>    

    <div id="content-wrapper">  

        <div class="container">
            <div class="empty-space marg-lg-b100 marg-sm-b50 marg-xs-b30"></div>

            <!-- TT-TITLE -->
            <div class="tt-title">
                <div class="tt-title-cat">Say hello</div>
                <h2 class="c-h2"><small>Contact Us</small></h2>
            </div>
            <div class="empty-space marg-lg-b35 marg-sm-b30"></div>

            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                    <div class="tt-contact-form">
                        <form onSubmit="return submitForm(this);" action="./" method="post" name="contactform">
                            <div class="row">
                                <div class="col-sm-6">
                                    <input class="c-input type-2 size-2" type="text" required="" name="name" placeholder="Your Name">
                                </div>
                                <div class="col-sm-6">
                                    <input class="c-input type-2 size-2" type="text" required="" name="email" placeholder="Your Email">
                                </div>
                            </div>
                            <textarea class="c-area type-2 size-2" required="" required="" name="message" placeholder="Your Message"></textarea>
                            <input class="c-btn type-1 size-3 full" type="submit" value="Talk to Us">
                            <br/><br/><br/>
                            <div style="padding: 20px" class="simple-text alert-success alert-email-success tt-request-success">
                                <p></p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>            

            <div class="empty-space marg-lg-b150 marg-sm-b50 marg-xs-b30"></div> 
        </div>
    </div>


    <!-- FOOTER -->


    <!-- JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <script src="js/idangerous.swiper.min.js"></script>
    <script src="js/isotope.pkgd.min.js"></script>


    <script src="js/global.js"></script>
    <script >
        function submitForm(obj) {
            $.ajax({type:'POST', url:'email-action.php', data:$(obj).serialize(), success: function(response) {
               $(obj).find('.tt-request-success').html('Your message was sent successfully').show();
               $(obj).append('<input type="reset" class="reset-button"/>');
            $('.reset-button').click().remove();
            }});                
            return false;
        }       
    </script>    
</body>
</html>
