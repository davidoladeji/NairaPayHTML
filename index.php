<?
session_start();
require('config.php');
require('data.php');
require('fungsi.php');
?>

<!DOCTYPE html>
<html>
<head lang="en">
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui"/>

    <!-- FONTS -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css"  rel="stylesheet" >
    <link href='https://fonts.googleapis.com/css?family=Poppins:300,400,700' rel='stylesheet' type='text/css'>

    <!-- CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous" rel="stylesheet">
    <link href="css/idangerous.swiper.css" rel="stylesheet" type="text/css" />
    <link href="css/mainstyle.css" rel="stylesheet" type="text/css" />

    <link rel="shortcut icon" href="img/favicon.ico" />
  	<title>Naira Pay Naija | Split-second payments</title>
</head>
<body>

    <!-- LOADER -->
    <div id="loader-wrapper">
        <div id="loader"><span class="loader-inner"></span></div>
        <div id="loading-text">LOADING</div>
    </div>

    <!-- HEADER -->
    <header class="tt-header">
        <div class="container">
            <div class="top-inner clearfix">
                <div class="top-inner-container">
                    <a class="logo" href="index.php"><img src="img/header/logo.png" height="52" width="71" alt=""></a>
                    <button class="cmn-toggle-switch"><span></span></button>
                </div>
            </div>
            <div class="toggle-block">
                <div class="toggle-block-container">
                    <nav class="main-nav clearfix">
                        <ul>
                            <li class="active">
                                <a href="index.php">Home</a>
                            </li>
                            <li class="parent">
                                <a href="#standout">About <i class="menu-toggle fa fa-angle-down"></i></a>
                            </li>
                            <li><a href="#packages">Packages</a></li>
                            <li><a href="#faq">FAQ</a></li>
                            <li><a href="contact.php">Contact</a></li>
                        </ul>
                    </nav>
                    <div class="nav-more">
                        <a class="c-btn type-2" href="/user/login"><span>Login</span></a>
                    </div>
                    <div class="nav-more">
                        <a class="c-btn type-2" href="/user/register"><span>Register</span></a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="tt-header-margin"></div>    

    <div id="content-wrapper">

        <!-- TT-MSLIDE -->
        <div class="swiper-container" data-autoplay="6000" data-spaceBetw0een="5000" data-loop="1" data-speed="15000" data-center="0" data-slides-per-view="1" data-add-slides="2">
            <div class="swiper-wrapper clearfix">
                <div class="swiper-slide active" data-val="0">
                    <div class="tt-mslide background-block" style="background-image:url(img/slider/bg.jpg);">
                        <div class="container">
                            <div class="tt-mslide-inner">
                                <h1 class="c-h1">Earn 200% from 20 minutes or less</h1>
                                <div class="simple-text size-2">
                                    <p>Yes, we’ll help you achieve your 2017 dreams and goals</p>
                                </div>
                                <a class="c-btn type-1" href="/user/register">Get Started Now</a>
                                <div class="empty-space marg-lg-b70 marg-md-b50"></div>
                                <div class="empty-space marg-lg-b200 marg-xs-b160"></div>      
                                <div class="empty-space marg-lg-b170 marg-md-b100 marg-sm-b50 marg-xs-b0"></div>
                                <div class="tt-mslide-img">
                                    <img class="img-responsive" src="img/slider/thumb-1.jpg" height="370" width="820" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide" data-val="1">
                    <div class="tt-mslide background-block" style="background-image:url(img/slider/bg.jpg);">
                        <div class="container">
                            <div class="tt-mslide-inner">
                                <h1 class="c-h1">It only takes a <b>BOLD</b> first step</h1>
                                <div class="simple-text size-2">
                                    <p>Yes, this is different!</p>
                                </div>
                                <a class="c-btn type-1" href="/user/register">Get Started Now</a>
                                <div class="empty-space marg-lg-b70 marg-md-b50 "></div>
                                <div class="empty-space marg-lg-b200 marg-xs-b160"></div>      
                                <div class="empty-space marg-lg-b170 marg-md-b100 marg-sm-b50 marg-xs-b0"></div>
                                <div class="tt-mslide-img">
                                    <img class="img-responsive" src="img/slider/thumb-2.jpg" height="370" width="820" alt="">
                                </div>
                            </div>
                        </div>
                    </div>                          
                </div>
            </div>
            <div class="pagination type-1 pos-1 visible-xs-block"></div>
            <div class="swiper-arrow-left tt-arrow-left type-1 pos-1 hidden-xs"><span class="lnr lnr-chevron-left"></span></div>
            <div class="swiper-arrow-right tt-arrow-right type-1 pos-1 hidden-xs"><span class="lnr lnr-chevron-right"></span></div>                   
        </div> 
        
        <div class="container">
            <div class="empty-space marg-lg-b95 marg-sm-b50"></div>
            
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    
                    <!-- TT-TITLE -->
                    <div class="tt-title">
                        <div class="tt-title-cat">investments made easy</div>
                        <h2 class="c-h2"><small>How it works</small></h2>
                    </div>
                    <div class="empty-space marg-lg-b40 marg-sm-b30"></div>

                    <!-- TT-TAB-WRAPPER TYPE-1 -->
                    <div class="tt-tab-wrapper type-1">
                        <div class="tt-tab-nav-wrapper">
                            <div class="tt-tab-select">
                                <div class="select-arrow"><i class="fa fa-angle-down"></i></div>
                                <select>
                                    <option selected="">Register</option>
                                    <option>Choose Plan</option>
                                    <option>Get Matched</option>
                                    <option>Get Paid</option>
                                    <option>Repeat</option>
                                </select>                            
                            </div>
                            <div  class="tt-nav-tab mbottom50">
                                <div class="tt-nav-tab-item active">
                                    <span class="lnr lnr-chart-bars"></span>
                                    <span class="tt-analitics-text">Register</span>
                                </div>
                                <div class="tt-nav-tab-item">
                                    <span class="lnr lnr-hand"></span>
                                    <span class="tt-analitics-text">Choose Plan</span>
                                </div>
                                <div class="tt-nav-tab-item">
                                    <span class="lnr lnr-pie-chart"></span>
                                    <span class="tt-analitics-text">Get Matched</span>
                                </div>
                                <div class="tt-nav-tab-item">
                                    <span class="lnr lnr-laptop-phone"></span>
                                    <span class="tt-analitics-text">Get Paid</span>
                                </div>
                                <div class="tt-nav-tab-item">
                                    <span class="lnr lnr-pencil"></span>
                                    <span class="tt-analitics-text">Repeat</span>
                                </div>        
                            </div>
                        </div>
                        <div class="tt-tabs-content clearfix mbottom50">
                            <div class="tt-tab-info active">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="simple-text size-3">
                                            <h3><small>Register now!</small></h3>
                                            <p>Do not wait to be told how NairaPayNaija works. Sign up yourself and experience the difference. All you need :</p>
                                            <ul>
                                                <li>Your Full name</li>
                                                <li>A special and unique username</li>
                                                <li>An Email address</li>
                                                <li>Your phone number</li>
                                                <li>Your Bank Account Details</li>
                                                <li>Click the Register Button</li>
                                            </ul>
                                        </div>
                                        <div class="empty-space marg-sm-b30"></div>
                                    </div>
                                    <div class="col-md-7">
                                        <img class="img-responsive" src="img/simple-image/img_1.jpg" height="333" width="605" alt="">
                                    </div>
                                </div>                                           
                            </div>
                            <div class="tt-tab-info">
                                <div class="row">
                                    <div class="col-md-7">
                                        <img class="img-responsive" src="img/simple-image/img_2.jg" height="333" width="605" alt="">
                                        <div class="empty-space marg-sm-b30"></div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="simple-text size-3">
                                            <h3><small>Pick a Plan</small></h3>
                                            <p>According to your donation so shall your returns be, in double.</p>

                                        </div>
                                    </div>                                
                                </div>                                    
                            </div>
                            <div class="tt-tab-info">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="simple-text size-3">
                                            <h3><small>Get Matched</small></h3>
                                            <p>Its simple as clicking the register button.</p>

                                        </div>
                                        <div class="empty-space marg-sm-b30"></div>
                                    </div>
                                    <div class="col-md-7">
                                        <img class="img-responsive" src="img/simple-image/img_3.png" height="333" width="605" alt="">
                                    </div>
                                </div>                                                     
                            </div>
                            <div class="tt-tab-info">
                                <div class="row">
                                    <div class="col-md-7">
                                        <img class="img-responsive" src="img/simple-image/img_4.png" height="333" width="605" alt="">
                                        <div class="empty-space marg-sm-b30"></div>
                                    </div>                            
                                    <div class="col-md-5">
                                        <div class="simple-text size-3">
                                            <h3><small>Get Paid</small></h3>
                                            <p>Each time you get paid double your donation.</p>

                                        </div>
                                    </div>
                                </div>      
                            </div>  
                            <div class="tt-tab-info">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="simple-text size-3">
                                            <h3><small>Repeat the Process</small></h3>
                                            <p>When success hits once, who wants it to stop?</p>

                                        </div>
                                        <div class="empty-space marg-sm-b30"></div>
                                    </div>
                                    <div class="col-md-7">
                                        <img class="img-responsive" src="img/simple-image/img_5.png" height="333" width="605" alt="">
                                    </div>
                                </div>                             
                            </div>     
                        </div>
                    </div>                     

                </div>
            </div>
            <div class="empty-space marg-lg-b110 marg-sm-b50"></div>
            
        </div>
        <div class="tt-devider"></div>

        <div class="container">
            <div class="empty-space marg-lg-b95 marg-sm-b50"></div>

            <div  id="standout" class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    
                    <!-- TT-TITLE -->
                    <div class="tt-title">
                        <div class="tt-title-cat">How we  stand out!</div>
                        <h2 class="c-h2"><small>NAIRA PAY NAIJA</small></h2>
                    </div>
                    <div class="empty-space marg-lg-b35 marg-sm-b30"></div>

                    <!-- TT-SERVICE -->
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="tt-service color-2 clearfix">
                                <a class="tt-service-icon" href="#">
                                    <span class="lnr lnr-rocket"></span>
                                </a>
                                <div class="tt-service-info">
                                    <a class="tt-service-title c-h4">Really really fast</a>
                                    <div class="simple-text size-3">
                                        <p>Like advertised, 20minutes averagely and you are matched</p>
                                    </div>
                                </div>
                            </div>
                            <div class="empty-space marg-xs-b30"></div>
                        </div>
                        <div class="col-sm-6">
                            <div class="tt-service color-2 clearfix">
                                <a class="tt-service-icon" href="#">
                                    <span class="lnr lnr-poop"></span>
                                </a>
                                <div class="tt-service-info">
                                    <a class="tt-service-title c-h4">No Bullshit Formulas</a>
                                    <div class="simple-text size-3">
                                        <p>Its you, your 1 donation and 2 donations in return.</p>
                                    </div>
                                </div>
                            </div>                    
                        </div>
                    </div>
                    <div class="empty-space marg-lg-b40 marg-sm-b30"></div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="tt-service color-2 clearfix">
                                <a class="tt-service-icon"  href="#">
                                    <span class="lnr lnr-chart-bars"></span>
                                </a>
                                <div class="tt-service-info">
                                    <a class="tt-service-title c-h4">Transparent System</a>
                                    <div class="simple-text size-3">
                                        <p>Donation system, where users help users, plain simple!</p>
                                    </div>
                                </div>
                            </div>
                            <div class="empty-space marg-xs-b30"></div>
                        </div>
                        <div class="col-sm-6">
                            <div class="tt-service color-2 clearfix">
                                <a class="tt-service-icon" href="#">
                                    <span class="lnr lnr-envelope"></span>
                                </a>
                                <div class="tt-service-info">
                                    <a class="tt-service-title c-h4">Email Support</a>
                                    <div class="simple-text size-3">
                                        <p> You can easily mail and admin or use contact form. </p>
                                    </div>
                                </div>
                            </div>                    
                        </div>
                    </div>
                    <div class="empty-space marg-lg-b40 marg-sm-b30"></div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="tt-service color-2 clearfix">
                                <a class="tt-service-icon" href="#">
                                    <span class="lnr lnr-diamond"></span>
                                </a>
                                <div class="tt-service-info">
                                        <a class="tt-service-title c-h4">Live chat</a>
                                    <div class="simple-text size-3">
                                        <p>In a hurry? talk to a live agent, like right now!</p>
                                    </div>
                                </div>
                            </div>
                            <div class="empty-space marg-xs-b30"></div>
                        </div>
                        <div class="col-sm-6">
                            <div class="tt-service color-2 clearfix">
                                <a class="tt-service-icon" href="#">
                                    <span class="lnr lnr-thumbs-up"></span>
                                </a>
                                <div class="tt-service-info">
                                    <a class="tt-service-title c-h4">Customer Support</a>
                                    <div class="simple-text size-3">
                                        <p>We have the best support system offered by any donation platform.</p>
                                    </div>
                                </div>
                            </div>                    
                        </div>
                    </div>

                </div>
            </div>

        </div>

        <div class="empty-space marg-lg-b95 marg-sm-b50 marg-xs-b30"></div>
        <div class="tt-devider"></div>
        <div class="empty-space marg-lg-b95 marg-sm-b50 marg-xs-b30"></div>


        <div class="container">
            <!-- TT-TITLE -->
            <div class="tt-title">
                <div class="tt-title-cat">happy participants</div>
                <h2 class="c-h2"><small>Don’t Just Take Our Word for It</small></h2>
            </div>
            <div class="empty-space marg-lg-b35 marg-sm-b30"></div>

            <!-- TT-TESTIMONIAL -->
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="swiper-container" data-autoplay="1" data-loop="1" data-speed="10000"   datadata-center="0" data-slides-per-view="1" data-add-slides="2">
                        <div class="swiper-wrapper clearfix">
                            <div class="swiper-slide active" data-val="0">
                                <div class="tt-testimonial clearfix">
                                    <div class="tt-testimonial-info">
                                        <div class="simple-text">
                                            <p>I am still in shock! This is wonderful. Thanks NairaPaynaija! I got a call in 5  mintes after being matched.</p>
                                        </div>
                                        <div class="tt-testimonial-label">
                                            - <a class="tt-testimonial-name" href="#">Waliyat Makinde</a>, <a href="#">Lagos</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide" data-val="1">
                                <div class="tt-testimonial clearfix">
                                    <div class="tt-testimonial-info">
                                        <div class="simple-text">
                                            <p>It doesn't get better than this. Na to dey PH multiple times oh. You guys are doing a great  job</p>
                                        </div>
                                        <div class="tt-testimonial-label">
                                            - <a class="tt-testimonial-name" href="#">Chuks Okafor</a>, <a href="#">Nnewi</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pagination type-1 visible-xs-block"></div>
                        <div class="tt-swiper-arrow-center">
                            <div class="swiper-arrow-left tt-arrow-left type-2 pos-2 hidden-xs"><span class="lnr lnr-chevron-left"></span></div>
                            <div class="swiper-arrow-right tt-arrow-right type-2 pos-2 hidden-xs"><span class="lnr lnr-chevron-right"></span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="empty-space marg-lg-b100 marg-sm-b50 marg-xs-b30"></div>
        <div class="tt-devider"></div>
        <div class="empty-space marg-lg-b95 marg-sm-b50 marg-xs-b30"></div>

        <div id="packages" class="container">
            <!-- TT-TITLE -->
            <div class="tt-title">
                <div class="tt-title-cat">simple as always</div>
                <h2 class="c-h2"><small>Simple Packages for Every Pocket</small></h2>
            </div>
            <div class="empty-space marg-lg-b40 marg-sm-b30"></div>         

            <!-- TT-PRICING -->
            <div class="row">
               <div class="col-sm-4">
                   <div class="tt-pricing">
                       <h4 class="tt-pricing-title">Basic Plan</h4>
                       <div class="tt-pricing-count">
                           <span class="tt-pricing-top">₦</span>5,000<span></span>
                       </div>
                       <ul class="tt-pricing-list">
                           <li class="active">2x1 Merge system</li>
                           <li class="active">Auto Assign</li>
                           <li class="active">Auto re-assign</li>
                       </ul>
                       <div class="tt-pricing-count-bottom">
                          ₦10000<span>return</span>
                       </div>
                   </div>
                   <div class="empty-space marg-lg-b40 marg-sm-b30"></div>
                   <div class="text-center">
                       <a class="c-btn type-1 size-2" href="/user/register">Start Now</a>
                   </div>
               </div>
               <div class="col-sm-4">
                   <div class="tt-pricing">
                       <h4 class="tt-pricing-title">Regular Plan</h4>
                       <div class="tt-pricing-count">
                           <span class="tt-pricing-top">₦</span>10,000<span></span>
                       </div>
                       <ul class="tt-pricing-list">
                           <li class="active">2x1 Merge system</li>
                           <li class="active">Auto Assign</li>
                           <li class="active">Auto re-assign</li>
                       </ul>
                       <div class="tt-pricing-count-bottom">
                           ₦20000<span>return</span>
                       </div>
                   </div>
                   <div class="empty-space marg-lg-b40 marg-sm-b30"></div>
                   <div class="text-center">
                       <a class="c-btn type-1 size-2" href="/user/register">Start Now</a>
                   </div>
               </div>

                <div class="col-sm-4">
                    <div class="tt-pricing">
                        <h4 class="tt-pricing-title">Ultimate Plan</h4>
                        <div class="tt-pricing-count">
                            <span class="tt-pricing-top">₦</span>20,000<span></span>
                        </div>
                        <ul class="tt-pricing-list">
                            <li class="active">2x1 Merge system</li>
                            <li class="active">Auto Assign</li>
                            <li class="active">Auto re-assign</li>
                        </ul>
                        <div class="tt-pricing-count-bottom">
                            ₦40,000<span>return</span>
                        </div>

                    </div>
                    <div class="empty-space marg-lg-b40 marg-sm-b30"></div>
                    <div class="text-center">
                        <a class="c-btn type-1 size-2" href="/user/register">Start Now</a>
                    </div>
                </div>
            </div>
            <div class="empty-space marg-lg-b40 marg-sm-b30"></div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="tt-pricing">
                        <h4 class="tt-pricing-title">Basic Plan</h4>
                        <div class="tt-pricing-count">
                            <span class="tt-pricing-top">₦</span>50,000<span></span>
                        </div>
                        <ul class="tt-pricing-list">
                            <li class="active">2x1 Merge system</li>
                            <li class="active">Auto Assign</li>
                            <li class="active">Auto re-assign</li>
                        </ul>
                        <div class="tt-pricing-count-bottom">
                            ₦100,000<span>return</span>
                        </div>
                    </div>
                    <div class="empty-space marg-lg-b40 marg-sm-b30"></div>
                    <div class="text-center">
                        <a class="c-btn type-1 size-2" href="/user/register">Start Now</a>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="tt-pricing">
                        <h4 class="tt-pricing-title">Regular Plan</h4>
                        <div class="tt-pricing-count">
                            <span class="tt-pricing-top">₦</span>100,000<span></span>
                        </div>
                        <ul class="tt-pricing-list">
                            <li class="active">2x1 Merge system</li>
                            <li class="active">Auto Assign</li>
                            <li class="active">Auto re-assign</li>
                        </ul>
                        <div class="tt-pricing-count-bottom">
                            ₦200,000<span>return</span>
                        </div>
                    </div>
                    <div class="empty-space marg-lg-b40 marg-sm-b30"></div>
                    <div class="text-center">
                        <a class="c-btn type-1 size-2" href="/user/register">Start Now</a>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="tt-pricing">
                        <h4 class="tt-pricing-title">Standard Plan</h4>
                        <div class="tt-pricing-count">
                            <span class="tt-pricing-top">₦</span>500,000<span></span>
                        </div>
                        <ul class="tt-pricing-list">
                            <li class="active">2x1 Merge system</li>
                            <li class="active">Auto Assign</li>
                            <li class="active">Auto re-assign</li>
                        </ul>
                        <div class="tt-pricing-count-bottom">
                            ₦1,000,000<span>return</span>
                        </div>
                    </div>
                    <div class="empty-space marg-lg-b40 marg-sm-b30"></div>
                    <div class="text-center">
                        <a class="c-btn type-1 size-2" href="/user/register">Start Now</a>
                    </div>
                </div>

            </div>




        </div>

        <div class="empty-space marg-lg-b95 marg-sm-b50 marg-xs-b30"></div>
        <div class="tt-devider"></div>
        <div class="empty-space marg-lg-b95 marg-sm-b50 marg-xs-b30"></div>


        <div class="empty-space marg-lg-b100 marg-sm-b50 marg-xs-b30"></div>
        <div class="tt-devider"></div>
        <div class="empty-space marg-lg-b100 marg-sm-b50 marg-xs-b30"></div>        

        <!--<div class="container">


            <div id="faq" class="container">
                TT-TITLE
                <div class="tt-title">
                    <div class="tt-title-cat">We know you might have questions</div>
                    <h2 class="c-h2"><small>Frequently Asked Questions</small></h2>
                </div>
                <div class="empty-space marg-lg-b40 marg-sm-b30"></div>

                TT-TAB-WRAPPER TYPE-1
                <div class="tt-tab-wrapper type-2">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="tt-tab-nav-wrapper">
                                <div class="tt-tab-select">
                                    <div class="select-arrow"><i class="fa fa-angle-down"></i></div>
                                    <select>
                                        <option selected="">Analytics</option>
                                        <option>Add On</option>
                                        <option>Infographics</option>
                                        <option>Marketing Tools</option>
                                        <option>Copywriting</option>
                                        <option>Pay Per Click</option>
                                    </select>
                                </div>
                                <div class="tt-nav-tab mbottom50">
                                    <div class="tt-nav-tab-item active">
                                        Digital Marketing
                                    </div>
                                    <div class="tt-nav-tab-item">
                                        Growth Hack
                                    </div>
                                    <div class="tt-nav-tab-item">
                                        Consultancy
                                    </div>
                                    <div class="tt-nav-tab-item">
                                        Social Marketing
                                    </div>
                                    <div class="tt-nav-tab-item">
                                        eBook Seminar
                                    </div>
                                    <div class="tt-nav-tab-item">
                                        Pay Per Click
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="tt-tabs-content clearfix mbottom50">
                                <div class="tt-tab-info active">
                                    <div class="empty-space marg-lg-b30 marg-sm-b0"></div>
                                    <div class="simple-text text-center">
                                        <p>Seed money optimized for social sharing chatvertizing brand awareness granular thought leadership. Engagement tweens native content drone.</p>
                                    </div>
                                    <div class="empty-space marg-lg-b10"></div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="tt-service type-3 clearfix">
                                                <a class="tt-service-icon" href="#">
                                                    <span class="lnr lnr-license"></span>
                                                </a>
                                                <div class="tt-service-info">
                                                    <a class="tt-service-title c-h4">Get Certified</a>
                                                    <div class="simple-text size-3">
                                                        <p>Mobile ready proprietary dedication intuitive. Thought leadership pass the clap hackathon wearables.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="tt-service type-3 clearfix">
                                                <a class="tt-service-icon" href="#">
                                                    <span class="lnr lnr-earth"></span>
                                                </a>
                                                <div class="tt-service-info">
                                                    <a class="tt-service-title c-h4">All Over the Web</a>
                                                    <div class="simple-text size-3">
                                                        <p>Platform omnichannel click bait thought leadership pivot. Disrupt taste makers council conversions emerging.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="empty-space marg-lg-b10"></div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="tt-service type-3 clearfix">
                                                <a class="tt-service-icon" href="#">
                                                    <span class="lnr lnr-pie-chart"></span>
                                                </a>
                                                <div class="tt-service-info">
                                                    <a class="tt-service-title c-h4">Analytics Made Easy</a>
                                                    <div class="simple-text size-3">
                                                        <p>Virality The Cloud content marketing thought leadership. Alignment platform phablet. CRM scalability mobile ready.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="tt-service type-3 clearfix">
                                                <a class="tt-service-icon" href="#">
                                                    <span class="lnr lnr-users"></span>
                                                </a>
                                                <div class="tt-service-info">
                                                    <a class="tt-service-title c-h4">Trusted by Clients</a>
                                                    <div class="simple-text size-3">
                                                        <p>Thought leadership iterative seed money lean content proprietary. Snackable content quiet period.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tt-tab-info">
                                    <div class="empty-space marg-lg-b30 marg-sm-b0"></div>
                                    <div class="simple-text text-center">
                                        <p>Seed money optimized for social sharing chatvertizing brand awareness granular thought leadership. Engagement tweens native content drone.</p>
                                    </div>
                                    <div class="empty-space marg-lg-b10"></div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="tt-service type-3 clearfix">
                                                <a class="tt-service-icon" href="#">
                                                    <span class="lnr lnr-pie-chart"></span>
                                                </a>
                                                <div class="tt-service-info">
                                                    <a class="tt-service-title c-h4">Analytics Made Easy</a>
                                                    <div class="simple-text size-3">
                                                        <p>Virality The Cloud content marketing thought leadership. Alignment platform phablet. CRM scalability mobile ready.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="tt-service type-3 clearfix">
                                                <a class="tt-service-icon" href="#">
                                                    <span class="lnr lnr-users"></span>
                                                </a>
                                                <div class="tt-service-info">
                                                    <a class="tt-service-title c-h4">Trusted by Clients</a>
                                                    <div class="simple-text size-3">
                                                        <p>Thought leadership iterative seed money lean content proprietary. Snackable content quiet period.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="empty-space marg-lg-b30"></div>
                                    <div class="simple-text text-center">
                                        <p>Seed money optimized for social sharing chatvertizing brand awareness granular thought leadership. Engagement tweens native content drone. Hit the like button CPM holistic content marketing responsive.</p>
                                    </div>
                                </div>
                                <div class="tt-tab-info">
                                    <div class="empty-space marg-lg-b30 marg-sm-b0"></div>
                                    <div class="simple-text text-center">
                                        <p>Seed money optimized for social sharing chatvertizing brand awareness granular thought leadership. Engagement tweens native content drone.</p>
                                    </div>
                                    <div class="empty-space marg-lg-b10"></div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="tt-service type-3 clearfix">
                                                <a class="tt-service-icon" href="#">
                                                    <span class="lnr lnr-license"></span>
                                                </a>
                                                <div class="tt-service-info">
                                                    <a class="tt-service-title c-h4">Get Certified</a>
                                                    <div class="simple-text size-3">
                                                        <p>Mobile ready proprietary dedication intuitive. Thought leadership pass the clap hackathon wearables.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="tt-service type-3 clearfix">
                                                <a class="tt-service-icon" href="#">
                                                    <span class="lnr lnr-earth"></span>
                                                </a>
                                                <div class="tt-service-info">
                                                    <a class="tt-service-title c-h4">All Over the Web</a>
                                                    <div class="simple-text size-3">
                                                        <p>Platform omnichannel click bait thought leadership pivot. Disrupt taste makers council conversions emerging.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="empty-space marg-lg-b10"></div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="tt-service type-3 clearfix">
                                                <a class="tt-service-icon" href="#">
                                                    <span class="lnr lnr-pie-chart"></span>
                                                </a>
                                                <div class="tt-service-info">
                                                    <a class="tt-service-title c-h4">Analytics Made Easy</a>
                                                    <div class="simple-text size-3">
                                                        <p>Virality The Cloud content marketing thought leadership. Alignment platform phablet. CRM scalability mobile ready.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="tt-service type-3 clearfix">
                                                <a class="tt-service-icon" href="#">
                                                    <span class="lnr lnr-users"></span>
                                                </a>
                                                <div class="tt-service-info">
                                                    <a class="tt-service-title c-h4">Trusted by Clients</a>
                                                    <div class="simple-text size-3">
                                                        <p>Thought leadership iterative seed money lean content proprietary. Snackable content quiet period.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tt-tab-info">
                                    <div class="empty-space marg-lg-b30 marg-sm-b0"></div>
                                    <div class="simple-text text-center">
                                        <p>Seed money optimized for social sharing chatvertizing brand awareness granular thought leadership. Engagement tweens native content drone.</p>
                                    </div>
                                    <div class="empty-space marg-lg-b10"></div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="tt-service type-3 clearfix">
                                                <a class="tt-service-icon" href="#">
                                                    <span class="lnr lnr-pie-chart"></span>
                                                </a>
                                                <div class="tt-service-info">
                                                    <a class="tt-service-title c-h4">Analytics Made Easy</a>
                                                    <div class="simple-text size-3">
                                                        <p>Virality The Cloud content marketing thought leadership. Alignment platform phablet. CRM scalability mobile ready.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="tt-service type-3 clearfix">
                                                <a class="tt-service-icon" href="#">
                                                    <span class="lnr lnr-users"></span>
                                                </a>
                                                <div class="tt-service-info">
                                                    <a class="tt-service-title c-h4">Trusted by Clients</a>
                                                    <div class="simple-text size-3">
                                                        <p>Thought leadership iterative seed money lean content proprietary. Snackable content quiet period.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="empty-space marg-lg-b30"></div>
                                    <div class="simple-text text-center">
                                        <p>Seed money optimized for social sharing chatvertizing brand awareness granular thought leadership. Engagement tweens native content drone. Hit the like button CPM holistic content marketing responsive.</p>
                                    </div>
                                </div>
                                <div class="tt-tab-info">
                                    <div class="empty-space marg-lg-b30 marg-sm-b0"></div>
                                    <div class="simple-text text-center">
                                        <p>Seed money optimized for social sharing chatvertizing brand awareness granular thought leadership. Engagement tweens native content drone.</p>
                                    </div>
                                    <div class="empty-space marg-lg-b10"></div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="tt-service type-3 clearfix">
                                                <a class="tt-service-icon" href="#">
                                                    <span class="lnr lnr-license"></span>
                                                </a>
                                                <div class="tt-service-info">
                                                    <a class="tt-service-title c-h4">Get Certified</a>
                                                    <div class="simple-text size-3">
                                                        <p>Mobile ready proprietary dedication intuitive. Thought leadership pass the clap hackathon wearables.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="tt-service type-3 clearfix">
                                                <a class="tt-service-icon" href="#">
                                                    <span class="lnr lnr-earth"></span>
                                                </a>
                                                <div class="tt-service-info">
                                                    <a class="tt-service-title c-h4">All Over the Web</a>
                                                    <div class="simple-text size-3">
                                                        <p>Platform omnichannel click bait thought leadership pivot. Disrupt taste makers council conversions emerging.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="empty-space marg-lg-b10"></div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="tt-service type-3 clearfix">
                                                <a class="tt-service-icon" href="#">
                                                    <span class="lnr lnr-pie-chart"></span>
                                                </a>
                                                <div class="tt-service-info">
                                                    <a class="tt-service-title c-h4">Analytics Made Easy</a>
                                                    <div class="simple-text size-3">
                                                        <p>Virality The Cloud content marketing thought leadership. Alignment platform phablet. CRM scalability mobile ready.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="tt-service type-3 clearfix">
                                                <a class="tt-service-icon" href="#">
                                                    <span class="lnr lnr-users"></span>
                                                </a>
                                                <div class="tt-service-info">
                                                    <a class="tt-service-title c-h4">Trusted by Clients</a>
                                                    <div class="simple-text size-3">
                                                        <p>Thought leadership iterative seed money lean content proprietary. Snackable content quiet period.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tt-tab-info">
                                    <div class="empty-space marg-lg-b30 marg-sm-b0"></div>
                                    <div class="simple-text text-center">
                                        <p>Seed money optimized for social sharing chatvertizing brand awareness granular thought leadership. Engagement tweens native content drone.</p>
                                    </div>
                                    <div class="empty-space marg-lg-b10"></div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="tt-service type-3 clearfix">
                                                <a class="tt-service-icon" href="#">
                                                    <span class="lnr lnr-pie-chart"></span>
                                                </a>
                                                <div class="tt-service-info">
                                                    <a class="tt-service-title c-h4">Analytics Made Easy</a>
                                                    <div class="simple-text size-3">
                                                        <p>Virality The Cloud content marketing thought leadership. Alignment platform phablet. CRM scalability mobile ready.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="tt-service type-3 clearfix">
                                                <a class="tt-service-icon" href="#">
                                                    <span class="lnr lnr-users"></span>
                                                </a>
                                                <div class="tt-service-info">
                                                    <a class="tt-service-title c-h4">Trusted by Clients</a>
                                                    <div class="simple-text size-3">
                                                        <p>Thought leadership iterative seed money lean content proprietary. Snackable content quiet period.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="empty-space marg-lg-b30"></div>
                                    <div class="simple-text text-center">
                                        <p>Seed money optimized for social sharing chatvertizing brand awareness granular thought leadership. Engagement tweens native content drone. Hit the like button CPM holistic content marketing responsive.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="empty-space marg-lg-b100 marg-sm-b50 marg-xs-b30"></div>
        </div>-->


    </div>


    <!-- FOOTER -->
    <footer class="tt-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-3">

                    <div class="tt-address clearfix">
                        <div class="tt-address-icon"><i class="fa fa-phone" aria-hidden="true"></i></div>
                        <div class="tt-address-info">
                            <a href="tel:123456789">+2348012324849</a>
                        </div>
                    </div>
                    <div class="empty-space marg-lg-b40 marg-xs-b30"></div>

                    <h5 class="c-h5 color-3">Get Social</h5>
                    <div class="empty-space marg-lg-b10"></div>
                    <ul class="tt-social clearfix">
                       <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                       <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                       <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                       <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                    </ul>
                    <div class="empty-space marg-sm-b30"></div>                       
                </div>
                <div class="col-xs-12 col-md-6">
                    <h5 class="c-h5 color-3">Drop us a line</h5>
                    <div class="empty-space marg-lg-b10"></div>
                    <div class="simple-text size-3 color-4">
                        <p>We'd  love to hear from you. Testimonials Questions and complaints can be sent here or use our support system</p>
                    </div>
                    <div class="empty-space marg-lg-b10"></div>

                    <div class="tt-footer-form">
                        <form method="post" onSubmit="return submitForm(this);" action="./" name="contactform">
                            <div class="row">
                                <div class="col-sm-6">
                                    <input class="c-input type-1" type="text" placeholder="Your Name" name="name" required>
                                </div>
                                <div class="col-sm-6">
                                    <input class="c-input type-1" type="email" placeholder="Your Email" name="email" required>
                                </div>
                            </div>
                            <textarea class="c-area type-1" placeholder="Your Message" name="message" required></textarea>
                            <div class="c-btn type-1">Submit<input type="submit"/></div>
                            <div class="simple-text light tt-request-success">
                                <p></p>
                            </div>
                        </form>
                    </div>                    
                </div>
            </div>
            <div class="empty-space marg-lg-b80 marg-sm-b50 marg-xs-b30"></div>
        </div>
        <div class="tt-footer-copy">
            <div class="container">
                <div class="simple-text size-5 color-4">
                    <p>© Built with pride and no caffeine. All rights reserved. <a target="_blank" href="#">NairaPayNaija™</a></p>
                </div>
            </div>
        </div>
    </footer>

    <!-- JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <script src="js/idangerous.swiper.min.js"></script>
    <script src="js/global.js"></script>

    <script >
        function submitForm(obj) {
            $.ajax({type:'POST', url:'email-action.php', data:$(obj).serialize(), success: function(response) {
               $(obj).find('.tt-request-success').html('Your message was sent successfully').show();
               $(obj).append('<input type="reset" class="reset-button"/>');
            $('.reset-button').click().remove();
            }});                
            return false;
        }       
    </script> 
</body>
</html>
